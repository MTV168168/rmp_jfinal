package com.dcits.dto;

import java.io.Serializable;
import java.util.List;

/**
 * description: todo
 *
 * @author sun jun
 * @className ResponseSocketDTO
 * @date 2020/9/21 15:31
 */
public class ResponseSocketDTO implements Serializable {

    private String host;
    private Integer viewId;
    private String commandResponse;

    public String getCommandResponse() {
        return commandResponse;
    }

    public void setCommandResponse(String commandResponse) {
        this.commandResponse = commandResponse;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getViewId() {
        return viewId;
    }

    public void setViewId(Integer viewId) {
        this.viewId = viewId;
    }

    public static ResponseSocketDTO generateResponse(Integer viewId, String host, String commandResponse) {
        ResponseSocketDTO responseSocketDTO = new ResponseSocketDTO();

        responseSocketDTO.setHost(host);
        responseSocketDTO.setCommandResponse(commandResponse);
        responseSocketDTO.setViewId(viewId);

        return responseSocketDTO;
    }
}
