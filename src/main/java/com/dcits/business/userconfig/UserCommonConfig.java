package com.dcits.business.userconfig;

import com.dcits.business.server.ServerType;
import org.apache.log4j.Logger;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class UserCommonConfig {
	
	private Map<String, Object> monitoring = new HashMap<String, Object>();
	private Map<String, Map<String, Object>> alert = new HashMap<String, Map<String, Object>>();
	private Map<String, Object> export = new HashMap<String, Object>();
	private Map<String, Object> other = new HashMap<String, Object>();
	
	private static final Logger logger = Logger.getLogger(UserCommonConfig.class);
	
	/*******************常用配置名称*********************************/
	public static final String refresh_tnterval_time = "refreshIntervalTime";
	public static final String play_notic_interval_time = "playNoticIntervalTime";
	public static final String max_info_data_count = "maxInfoDataCount";
	public static final String auto_clear_data_flag = "autoClearDataFlag";
	public static final String auto_save_data_flag = "autoSaveDataFlag";
	public static final String alert_monitor_interval_time = "alertMonitorIntervalTime";
	public static final String cache_data_detection_interval_time = "cacheDataDetectionIntervalTime";	
	public static final String alert_info_tip_show = "alertInfoTipShow";	
	
	public static final String other_website_url = "home";
	public static final String other_xuqiu_api_url = "apiUrl";

	public static UserCommonConfig defaultSettingInstance() {
		UserCommonConfig config = new UserCommonConfig();
		config.getMonitoring().put(refresh_tnterval_time, 6000);
		config.getMonitoring().put(play_notic_interval_time, 4000);
		config.getMonitoring().put(max_info_data_count, 20000);
		config.getMonitoring().put(auto_clear_data_flag, false);
		config.getMonitoring().put(auto_save_data_flag, true);
		config.getMonitoring().put(alert_monitor_interval_time, 10000);
		config.getMonitoring().put(cache_data_detection_interval_time, 30000);
		
		config.getOther().put(alert_info_tip_show, true);
		config.getOther().put(other_website_url, "");
		config.getOther().put(other_xuqiu_api_url, "");
		
		for (String typeName:ServerType.typeClasses.keySet()) {
			Class clazz = ServerType.typeClasses.get(typeName);;
			try {				
				Field f = clazz.getDeclaredField("alertThreshold");
				f.setAccessible(true);
				config.getAlert().put(typeName, (Map<String, Object>) f.get(clazz));
			} catch (Exception e) {
				logger.error(typeName + "类型没有配置预警类目或者配置错误!", e);
			}
		}
		
		
		return config;
	}
	
	
	public Map<String, Object> getMonitoring() {
		return monitoring;
	}

	public void setMonitoring(Map<String, Object> monitoring) {
		this.monitoring = monitoring;
	}

	public Map<String, Map<String, Object>> getAlert() {
		return alert;
	}

	public void setAlert(Map<String, Map<String, Object>> alert) {
		this.alert = alert;
	}

	public Map<String, Object> getExport() {
		return export;
	}

	public void setExport(Map<String, Object> export) {
		this.export = export;
	}

	public Map<String, Object> getOther() {
		return other;
	}

	public void setOther(Map<String, Object> other) {
		this.other = other;
	}

	@Override
	public String toString() {
		return "UserCommonConfig [monitoring=" + monitoring + ", alert=" + alert + ", export=" + export + ", other="
				+ other + "]";
	}	
	
}
