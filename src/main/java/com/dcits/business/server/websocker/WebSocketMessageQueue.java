package com.dcits.business.server.websocker;

import com.dcits.dto.ResponseSocketDTO;

import javax.websocket.Session;

/**
 * description: todo
 *
 * @author sun jun
 * @className WebSocketMessageQueue
 * @date 2020/9/27 11:35
 */
public class WebSocketMessageQueue {

    private Session session;

    private ResponseSocketDTO responseSocketDTO;

    private String tag;

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public ResponseSocketDTO getResponseSocketDTO() {
        return responseSocketDTO;
    }

    public void setResponseSocketDTO(ResponseSocketDTO responseSocketDTO) {
        this.responseSocketDTO = responseSocketDTO;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
