package com.dcits.business.server.linux.parse;

import com.dcits.business.server.linux.LinuxMonitoringInfo;

public class HPUXParseInfo extends ParseInfo{
	
	protected HPUXParseInfo() {
		
	}

	@Override
	public String parseMountDevice(String info, LinuxMonitoringInfo monitoringInfo) {
        return "true";
		
	}

	@Override
	public String parseDeviceIOInfo(String info, LinuxMonitoringInfo monitoringInfo) {
		return "true";
	}
}
