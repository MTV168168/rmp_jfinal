package com.dcits.constant;

public class ConstantGlobalAttributeName {
	
	public static final String USER_SPACE_ATTRITURE_NAME = "userSpaces";
	
	public static final String LOGIN_USER = "loginUser";

	public static final String SSO_LOGIN_TOKEN = "sso";
}
